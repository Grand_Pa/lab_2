﻿namespace lab2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.dataGridView_X_Y = new System.Windows.Forms.DataGridView();
            this.button_Go = new System.Windows.Forms.Button();
            this.button_reload = new System.Windows.Forms.Button();
            this.textBox_base_langs = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chart_graphic = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.textBox_answer = new System.Windows.Forms.TextBox();
            this.textBox_cor_value = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_X_Y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_graphic)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_X_Y
            // 
            this.dataGridView_X_Y.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_X_Y.Location = new System.Drawing.Point(12, 26);
            this.dataGridView_X_Y.Name = "dataGridView_X_Y";
            this.dataGridView_X_Y.RowHeadersVisible = false;
            this.dataGridView_X_Y.Size = new System.Drawing.Size(200, 260);
            this.dataGridView_X_Y.TabIndex = 0;
            // 
            // button_Go
            // 
            this.button_Go.Location = new System.Drawing.Point(232, 26);
            this.button_Go.Name = "button_Go";
            this.button_Go.Size = new System.Drawing.Size(38, 260);
            this.button_Go.TabIndex = 3;
            this.button_Go.Text = "Go";
            this.button_Go.UseVisualStyleBackColor = true;
            this.button_Go.Click += new System.EventHandler(this.button_Go_Click);
            // 
            // button_reload
            // 
            this.button_reload.Location = new System.Drawing.Point(759, 316);
            this.button_reload.Name = "button_reload";
            this.button_reload.Size = new System.Drawing.Size(75, 23);
            this.button_reload.TabIndex = 5;
            this.button_reload.Text = "Reload";
            this.button_reload.UseVisualStyleBackColor = true;
            this.button_reload.Click += new System.EventHandler(this.button_reload_Click);
            // 
            // textBox_base_langs
            // 
            this.textBox_base_langs.Location = new System.Drawing.Point(37, 314);
            this.textBox_base_langs.Name = "textBox_base_langs";
            this.textBox_base_langs.Size = new System.Drawing.Size(100, 20);
            this.textBox_base_langs.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 298);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Base_langs";
            // 
            // chart_graphic
            // 
            chartArea1.Name = "ChartArea1";
            this.chart_graphic.ChartAreas.Add(chartArea1);
            legend1.Enabled = false;
            legend1.Name = "Legend1";
            this.chart_graphic.Legends.Add(legend1);
            this.chart_graphic.Location = new System.Drawing.Point(307, 26);
            this.chart_graphic.Name = "chart_graphic";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series1.IsVisibleInLegend = false;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.IsVisibleInLegend = false;
            series2.Legend = "Legend1";
            series2.Name = "Series2";
            this.chart_graphic.Series.Add(series1);
            this.chart_graphic.Series.Add(series2);
            this.chart_graphic.Size = new System.Drawing.Size(527, 260);
            this.chart_graphic.TabIndex = 8;
            this.chart_graphic.Text = "chart1";
            // 
            // textBox_answer
            // 
            this.textBox_answer.Location = new System.Drawing.Point(307, 314);
            this.textBox_answer.Multiline = true;
            this.textBox_answer.Name = "textBox_answer";
            this.textBox_answer.ReadOnly = true;
            this.textBox_answer.Size = new System.Drawing.Size(446, 25);
            this.textBox_answer.TabIndex = 9;
            // 
            // textBox_cor_value
            // 
            this.textBox_cor_value.Location = new System.Drawing.Point(158, 314);
            this.textBox_cor_value.Name = "textBox_cor_value";
            this.textBox_cor_value.Size = new System.Drawing.Size(100, 20);
            this.textBox_cor_value.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(199, 298);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "n^2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 361);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_cor_value);
            this.Controls.Add(this.textBox_answer);
            this.Controls.Add(this.chart_graphic);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_base_langs);
            this.Controls.Add(this.button_reload);
            this.Controls.Add(this.button_Go);
            this.Controls.Add(this.dataGridView_X_Y);
            this.Name = "Form1";
            this.Text = "Метод наименьших квадратов";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_X_Y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_graphic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_X_Y;
        private System.Windows.Forms.Button button_Go;
        private System.Windows.Forms.Button button_reload;
        private System.Windows.Forms.TextBox textBox_base_langs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_graphic;
        private System.Windows.Forms.TextBox textBox_answer;
        private System.Windows.Forms.TextBox textBox_cor_value;
        private System.Windows.Forms.Label label1;
    }
}

