﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace lab2
{
    public partial class Form1 : Form
    {

        double[,] Fb;
        double[,] SLAR;
        double[] Right_side;
        int Base_langs;
        int Count_value;
        double[] Value_of_uknown_arguments;
        List<double> list_of_X;
        List<double> list_of_Y;
        List<double> list_about_value_of_shag_X;
        public Form1()
        {
            InitializeComponent();
            dataGridView_X_Y.RowCount = 1;
            dataGridView_X_Y.ColumnCount = 2;
            int number = 0;
            foreach (DataGridViewColumn col in dataGridView_X_Y.Columns)
            {
                if (number == 0)
                {
                    col.HeaderText = Convert.ToString("X");
                }
                if (number == 1)
                {
                    col.HeaderText = Convert.ToString("Y");
                }
                number++;
            }
            dataGridView_X_Y.AutoResizeRows();
        }

        private void Calculate_Fb()
        {
            Base_langs = Convert.ToInt32(textBox_base_langs.Text)+1;
            Count_value = Convert.ToInt32(dataGridView_X_Y.Rows.Count-1);
            Fb = new double[Base_langs, Count_value];

            for (int i = 0; i < Count_value; i++)
            {
                Fb[0, i] = 1;
            }


            for (int i=1;i<Base_langs;i++)
            {
                for(int j=0;j<Count_value;j++)
                {
                    double Valuse_of_X = Convert.ToDouble(dataGridView_X_Y.Rows[j].Cells[0].Value);
                    Fb[i, j] += Valuse_of_X * Fb[i - 1, j];
                }
            }
        }

        private void Build_SLAR()
        {
            SLAR = new double[Base_langs, Base_langs]; // матрица СЛАР
            Right_side = new double[Base_langs]; // правая часть системы

            for(int i=0;i<Base_langs;i++)
            {
                Right_side[i] = 0;
                for(int j=0;j<Count_value;j++)
                {
                    int y = Convert.ToInt32(dataGridView_X_Y.Rows[j].Cells[1].Value);
                    Right_side[i] += Fb[i, j] * y;
                }

                for(int k=0;k<Base_langs;k++)
                {
                    SLAR[i, k] = 0;
                    for(int j=0;j<Count_value;j++)
                    {
                        SLAR[i, k] += Fb[i, j] * Fb[k, j];
                        if(i==0 && k==0)
                        {
                            break;
                        }
                    }
                }
            }
        }

        private void Find_f_x()
        {
            double max;
            int k, index;
            const double eps = 0.00001;  // точность
            Value_of_uknown_arguments = new double[Base_langs];
            k = 0;
            while (k < Base_langs)
            {
                // Поиск строки с максимальным a[i][k]
                max = Math.Abs(SLAR[k,k]);
                index = k;
                for (int i = k + 1; i < Base_langs; i++)
                {
                    if (Math.Abs(SLAR[i,k]) > max)
                    {
                        max = Math.Abs(SLAR[i,k]);
                        index = i;
                    }
                }
                // Перестановка строк

                for (int j = 0; j < Base_langs; j++)
                {
                    double temp = SLAR[k,j];
                    SLAR[k,j] = SLAR[index,j];
                    SLAR[index,j] = temp;
                }
                double tmp = Right_side[k];
                Right_side[k] = Right_side[index];
                Right_side[index] = tmp;
                // Нормализация уравнений
                for (int i = k; i < Base_langs; i++)
                {
                    double temp = SLAR[i,k];
                    if (Math.Abs(temp) < eps) continue; // для нулевого коэффициента пропустить
                    for (int j = 0; j < Base_langs; j++)
                        SLAR[i,j] = SLAR[i,j] / temp;
                    Right_side[i] = Right_side[i] / temp;
                    if (i == k) continue; // уравнение не вычитать само из себя
                    for (int j = 0; j < Base_langs; j++)
                        SLAR[i,j] = SLAR[i,j] - SLAR[k,j];
                    Right_side[i] = Right_side[i] - Right_side[k];
                }
                k++;
            }
            // обратная подстановка
            for (k = Base_langs - 1; k >= 0; k--)
            {
                Value_of_uknown_arguments[k] = Math.Round(Right_side[k],3);
                for (int i = 0; i < k; i++)
                    Right_side[i] = Right_side[i] - SLAR[i,k] * Value_of_uknown_arguments[k];
            }
        }

        private void Build_table_X_Y()
        {
            chart_graphic.Series[0].Points.Clear();
            chart_graphic.Series[1].Points.Clear();
            list_of_X = new List<double>();
            list_of_Y = new List<double>();
            for(int i=0;i<dataGridView_X_Y.Rows.Count-1;i++) // считать с таблицы данные
            {
                double value_of_x = Convert.ToDouble(dataGridView_X_Y.Rows[i].Cells[0].Value);
                double value_of_y = Convert.ToDouble(dataGridView_X_Y.Rows[i].Cells[1].Value);
                list_of_X.Add(value_of_x);
                list_of_Y.Add(value_of_y);
            }

            for(int i=0;i<list_of_X.Count;i++)
            {
                chart_graphic.Series[0].Points.AddXY(list_of_X[i], list_of_Y[i]);
            }
        }

        private void Build_function()
        {
            list_about_value_of_shag_X = new List<double>();
            double shag = list_of_X[list_of_X.Count - 1]/1000;
            double first_value = list_of_X[0];
            while(first_value<=list_of_X[list_of_X.Count-1])
            {
                double value_of_function = 0;
                for (int i=0;i<Value_of_uknown_arguments.Length;i++)
                {
                    if (i == 0)
                    {
                        value_of_function += Value_of_uknown_arguments[i] * 1;
                    }
                    else
                    {
                        value_of_function += Value_of_uknown_arguments[i] * Math.Pow(first_value,i);
                    }
                }
                value_of_function = Math.Round(value_of_function, 2, MidpointRounding.AwayFromZero);
                chart_graphic.Series[1].Points.AddXY(first_value, value_of_function);
                list_about_value_of_shag_X.Add(value_of_function);
                first_value = first_value + shag;
            }

            string line = "f(x) = ";

            for(int i=0;i<Value_of_uknown_arguments.Length;i++)
            {
                if(i!=Value_of_uknown_arguments.Length-1)
                {
                    line += Convert.ToString(Value_of_uknown_arguments[i] + "*x^" + i+" + ");
                }
                else
                {
                    line += Convert.ToString(Value_of_uknown_arguments[i] + "*x^" + i);
                }
                
            }

            textBox_answer.Text = line;
        }

        private void button_reload_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void Calculate_colar_vidnosh()
        {
            double mf = 0;
            double n = Count_value;
            double n_2;
            double chislitel, znamenatel;
            chislitel = znamenatel = 0;

            mf = ((1 / n) * Right_side[0]);

            for (int i=0;i<list_about_value_of_shag_X.Count;i++)
            {
                chislitel += Math.Pow((list_about_value_of_shag_X[i] - mf),2);
            }

            for(int i=0;i<list_of_Y.Count;i++)
            {
                znamenatel += Math.Pow((list_of_Y[i] - mf), 2);
            }

            n_2 = chislitel / znamenatel;
            n_2 = Math.Sqrt(n_2);
            textBox_cor_value.Text = Convert.ToString(n_2);
        }

        private void button_Go_Click(object sender, EventArgs e)
        {
            Calculate_Fb();
            Build_SLAR();
            Find_f_x();
            Build_table_X_Y();
            Build_function();
            Calculate_colar_vidnosh();
        }
    }
}
